import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def testData = TestDataFactory.findTestData('Data Files/Register Test Data')

for(def row = 1; row <= testData.getRowNumbers(); row++) {
	def name = testData.getValue('Name', row)
	def birthday = testData.getValue('Birthday', row)
	def email = testData.getValue('Email', row)
	def phone = testData.getValue('Phone', row)
	def password = testData.getValue('Password', row)
	def konfirmPassword = testData.getValue('KonfirmPassword', row)
	
	WebUI.openBrowser('')
	
	WebUI.maximizeWindow()
	
	WebUI.navigateToUrl('https://demo-app.online/daftar?')
	
	WebUI.verifyElementText(findTestObject('Register/label_Nama'), 'Nama')
	
	WebUI.setText(findTestObject('Object Repository/Register/inputFullname'), name)
	
	WebUI.verifyElementText(findTestObject('Register/label_Tanggal lahir'), 'Tanggal lahir')
	
	WebUI.setText(findTestObject('Object Repository/Register/inputBirthDay'), birthday)
	
	WebUI.verifyElementText(findTestObject('Register/label_E-Mail'), 'E-Mail')
	
	WebUI.setText(findTestObject('Object Repository/Register/inputEmail'), email)
	
	WebUI.verifyElementText(findTestObject('Register/label_Whatsapp'), 'Whatsapp')
	
	WebUI.setText(findTestObject('Object Repository/Register/inputPhone'), phone)
	
	WebUI.verifyElementText(findTestObject('Register/label_Kata Sandi'), 'Kata Sandi')
	
	WebUI.setEncryptedText(findTestObject('Object Repository/Register/inputPassword'), password)
	
	WebUI.verifyElementText(findTestObject('Register/label_Konfirmasi kata sandi'), 'Konfirmasi kata sandi')
	
	WebUI.setEncryptedText(findTestObject('Object Repository/Register/inputKonfirmasiPassword'), konfirmPassword)
	
	WebUI.click(findTestObject('Object Repository/Register/checkboxKonfirm'))
	
	WebUI.delay(3)
	
	WebUI.verifyElementText(findTestObject('Object Repository/Register/buttonDaftar'), 'Daftar')
	
	WebUI.click(findTestObject('Object Repository/Register/buttonDaftar'))
	
	WebUI.delay(3)
	
	WebUI.verifyElementText(findTestObject('Register/spanVerifikasiEmail'), 'Verifikasi Email')
	
	WebUI.delay(3)
	
	WebUI.closeBrowser()
}