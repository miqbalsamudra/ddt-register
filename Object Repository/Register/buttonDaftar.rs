<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonDaftar</name>
   <tag></tag>
   <elementGuidId>dc923cd9-5112-442c-a497-fdc2f5a84f1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonRegisterTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonRegisterTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5fdf4db7-7089-4861-a9bf-dc304523034b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonRegisterTrack</value>
      <webElementGuid>49c1bccc-d71a-4d17-8c95-4f474cb181e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>414d86b9-d447-4ce7-ba24-2b5e7eaa3732</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>939d431f-334f-4639-a5dc-485471a52350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Daftar
                                        </value>
      <webElementGuid>9c947ee6-a213-42b8-bda7-f4026d6da8a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonRegisterTrack&quot;)</value>
      <webElementGuid>606416a5-defb-43f9-bc9a-1b17cd9c7073</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonRegisterTrack']</value>
      <webElementGuid>24f75d7f-59e0-4a39-9a88-39029b1aff79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[1]</value>
      <webElementGuid>d84f8df9-75f3-4c67-a9be-565c76554776</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konfirmasi kata sandi'])[1]/following::button[1]</value>
      <webElementGuid>90060a1d-8d4c-4f13-bd01-c273b5f31a87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>609ab909-6d2d-4a3b-a088-90309dbb4e0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/button</value>
      <webElementGuid>088093c1-ebef-4c18-8e7f-0352ab90108b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonRegisterTrack' and @type = 'submit' and (text() = '
                                            Daftar
                                        ' or . = '
                                            Daftar
                                        ')]</value>
      <webElementGuid>c9466b93-6cc8-4341-b6c3-1b6f46c97fac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
